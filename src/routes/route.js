import React from 'react';
import { BrowserRouter as Router, Route, useHistory } from 'react-router-dom';
import {App, ShipperAddress, ShipperDashboard, ShipperShipment, ShipperWallet, TravellerDashboard, TravellerTrips, TravellerWallet, AuthPage } from "../pages";


const routing = (
  <Router history={useHistory}>
    <div>
      <Route exact path="/" component={App} />
      <Route path="/ShipperAddress" component={ShipperAddress} />
      <Route path="/ShipperDashboard" component={ShipperDashboard} />
      <Route path="/ShipperShipment" component={ShipperShipment} />
      <Route path="/ShipperWallet" component={ShipperWallet} />
      <Route path="/TravellerDashboard" component={TravellerDashboard} />
      <Route path="/TravellerTrips" component={TravellerTrips} />
      <Route path="/TravellerWallet" component={TravellerWallet} />
      <Route path="/Login" component={AuthPage} />
      <Route path="/Register" component={AuthPage} />
      <Route path="/Forgot" component={AuthPage} />


    </div>
  </Router>
);

export default routing;
