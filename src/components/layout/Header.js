import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Avatar, AppBar, Toolbar, Typography, Button } from '@material-ui/core';
import { NavLink, Link, withRouter } from 'react-router-dom';
import {Cached, Person} from '@material-ui/icons'
import Drawer from './Drawer'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    color: 'white',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 6,
  }
}));


const TypeSwitch = ({props, classes}) => {
  const thelink = props.location.pathname
  var wit = window.outerWidth
  const TheButton = (tolink, btnContent) => {
    return (
      <React.Fragment>
        <Link to={tolink} style={{color: 'white'}}>
          <Button variant="contained"  color="info" size="small">
            <Cached />
            <Typography  variant="caption" className={classes.title} style={{textTransform: 'capitalize'}}>
            { wit > 500 ? 'Switch to' : null} {btnContent}
            </Typography>
          </Button>
        </Link>
        {wit > 500 ?
        (<div style={{marginLeft: '10px'}}>
          <Grid container className={classes.root} spacing={1} direction="row" justify="center" alignItems="center" >
            <Grid item sm={4}>
              <Avatar className={classes.rounded}>
                <Person />
              </Avatar>
            </Grid>
            <Grid item sm>
              <Typography style={{paddingLeft: '10px'}} variant="caption" className={classes.title}>
                John Doe
              </Typography>
            </Grid>
          </Grid>
        </div> ): null}
    </React.Fragment>
    )}
  return thelink === "/TravellerDashboard" || thelink === "/TravellerTrips" || thelink === "/TravellerWallet" ?
    TheButton("/ShipperDashboard", "Shipper") :
    TheButton("/TravellerDashboard", "Traveller")
}


  // const Switcher = ({props, classes}) =>{
  //   var wit = window.outerWidth
  //   console.log(wit);

  //   return (
  //     wit > 500 ? <TypeSwitch props={props} classes={classes} /> : null
  //   )

  // }




function ButtonAppBar(props) {
  const classes = useStyles();
  const thelink = props.location.pathname
  const redirector = () => {
    props.history.push("/Login")
  }
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Drawer/>
          <Typography variant="h6" className={classes.title}>
            <NavLink to="/" style={{color: 'white', textDecoration: 'none'}}>
              Cairy Go
            </NavLink>
          </Typography>
          <div className={classes.grow} />
          {thelink === "/" || thelink === "/Login" || thelink === "/Register" || thelink === "/Forgot"? (
            thelink === "/" ? (
              <Button variant="contained" size="small" onClick={redirector} color="info" style={{ color: 'blue', textTransform: 'capitalize'}}>
                Login
              </Button>
            ) : null
          ) :  <TypeSwitch props={props} classes={classes} />}
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withRouter(ButtonAppBar)
