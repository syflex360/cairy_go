import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Drawer, List,  ListItem, ListItemIcon, ListItemText, IconButton } from '@material-ui/core';

import { Dashboard, PermContactCalendar, Flight, AccountBalanceWallet, LocalShipping, Menu } from '@material-ui/icons';
import { withRouter } from 'react-router-dom';
import {routLink} from '../mixings'

//
const drawerWidth = 40;
const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});

function TemporaryDrawer(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer = (side, open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideContent = {
    Trips:[
      {
        name:'Dashboard',
        icon: <Dashboard/>,
        link: ()=>{routLink(props, "/TravellerDashboard")}
      },
      {
        name:'Trips',
        icon: <Flight/>,
        link: ()=>{routLink(props, "/TravellerTrips")}
      },
      {
        name:'Wallet',
        icon: <AccountBalanceWallet/>,
        link: ()=>{routLink(props, "/TravellerWallet")}
      }
    ],

    ship:[
      {
        name:'Dashboard',
        icon: <Dashboard/>,
        link: ()=>{routLink(props, "/ShipperDashboard")}
      },
      {
        name:'Shipment',
        icon: <LocalShipping/>,
        link: ()=>{routLink(props, "/ShipperShipment")}
      },
      {
        name:'Contacts',
        icon: <PermContactCalendar/>,
        link: ()=>{routLink(props, "/ShipperAddress")}
      },
      {
        name:'Wallet',
        icon: <AccountBalanceWallet/>,
        link: ()=>{routLink(props, "/ShipperWallet")}
      },
      ]
  }
  const link = props.location.pathname

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
      {(link === "/TravellerDashboard" || link === "/TravellerTrips" || link === "/TravellerWallet"?sideContent.Trips:sideContent.ship).map(({name , icon, link}) =>(
        <ListItem button onClick={link}>
          <ListItemIcon>{icon}</ListItemIcon>
          <ListItemText>{name}</ListItemText>
        </ListItem>
      ))}
      </List>
    </div>
  );

  return (
    <div>
      <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={toggleDrawer('left', true)}>
        <Menu />
      </IconButton>
      <Drawer
        open={state.left}
        onClose={toggleDrawer('left', false)}
        anchor="left"
      >
        {sideList('left')}
      </Drawer>
    </div>
  );
}

export default withRouter(TemporaryDrawer)
