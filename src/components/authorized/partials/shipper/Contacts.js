import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Divider, List, ListItem, ListItemSecondaryAction, ListItemText, ListItemAvatar } from '@material-ui/core';
// import {AwaitingPayment, Recent} from './partials/order';
import { Top, Singular } from './partials'


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function SimpleTabs() {
  const classes = useStyles();
  const myContacts = [
    {
      name: 'John Doe',
      phone : '08033221156'
    },
    {
      name: 'John Maleek',
      phone : '08033221156'
    },
    {
      name: 'John Doe',
      phone : '08033221156'
    },
  ]

  return (
    <div className={classes.root}>
      <Top />
      <div style={{padding : '10px', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
        <List dense={true} style={{minWidth: '350px'}}>
          {myContacts.map(({name, phone})=>(
            <div>
              <Divider />
              <ListItem elevation={3} style={{backgroundColor: '#dff4f3', marginTop: '5px'}}>
                <ListItemAvatar>
                  <Avatar
                    src={'https://cdn3.iconfinder.com/data/icons/avatars-15/64/_Ninja-2-512.png'}
                  />
                </ListItemAvatar>
                <ListItemText
                  primary={name}
                  secondary={phone}
                />
                <ListItemSecondaryAction>
                  {/* <Button variant="outline" color="primary">
                    View
                  </Button> */}
                  <Singular/>
                </ListItemSecondaryAction>
              </ListItem>
            </div>
          ))}
        </List>
      </div>
    </div>
  );
}
