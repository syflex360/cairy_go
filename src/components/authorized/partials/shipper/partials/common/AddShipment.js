import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Toolbar, Button, TextField,Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, AppBar } from '@material-ui/core';
import { LocalShipping, Close } from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(0),
    },
  },
  grow: {
    flexGrow: 1,
  },
}));



const TheForm = ({classes}) =>{
  return(
    <React.Fragment>
      
      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" style={{padding: '20px'}}>
      <TextField autoFocus margin="dense" id="name" label="Parcel Name" type="text" fullWidth />
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Parcel Type" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Parcel Value" type="text" fullWidth />
          </Grid>
        </Grid>
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Parcel Quantity" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Parcel Weight" type="text" fullWidth />
          </Grid>
        </Grid>
        
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Destination Country" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Destination City" type="text" fullWidth />
          </Grid>
        </Grid>
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Destination Zip" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Destination Address" type="text" fullWidth />
          </Grid>
        </Grid>
      

      
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Recievers Name" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Recievers Phone" type="text" fullWidth />
          </Grid>
        </Grid>
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Arrival Airport" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Arrival Time" type="text" fullWidth />
          </Grid>
        </Grid>
        
      </Grid>

    </React.Fragment>

  )
}

const DialogHead = ({theCloser, classes })=>{
  return(
    <div className={classes.root}>
      <AppBar position="relative" elevation={0} color="transparent">
        <Toolbar className={classes.tools}>
          <div style={{textTransform: 'capitalize'}}>Ship</div>
          <div className={classes.grow} />
          <Button onClick={theCloser} size="small" color="transparent" style={{ color: 'red'}}>
            <Close />
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default function FormDialog() {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleClickOpen} style={{textTransform: 'capitalize'}}>
        <LocalShipping style={{paddingRight: "5px"}} />
        Ship
      </Button>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          <DialogHead theCloser={handleClose} classes={classes}/>

        </DialogTitle>
        <DialogContent>
          <DialogContentText> Put in the Details of your cargo/parcels below </DialogContentText>
          <TheForm classes={classes}  />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
