import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemSecondaryAction, ListItemText, ListItemIcon, Grid, Typography, Toolbar, Button, IconButton, TextField, Dialog, DialogActions, DialogContent, DialogTitle, AppBar, Divider } from '@material-ui/core';
import { Close, Payment, Receipt, Add } from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  grow: {
    flexGrow: 1,
  },
}));

const DialogHead = (props)=>{
  const classes = useStyles();
  const {theCloser} = props;
  return(
    <div className={classes.root}>
      <AppBar position="relative" elevation={0} color="transparent">
        <Toolbar className={classes.tools}>
          <div>Withdraw</div>
          <div className={classes.grow} />
          <IconButton onClick={theCloser} size="small" variant="contained" style={{ color: 'white', backgroundColor: 'red'}}>
            <Close />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  )
}

const TheForm = ({classes}) =>{
  return(
    <React.Fragment>
      
      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" style={{padding: '20px'}}>
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Bank" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Account Number" type="text" fullWidth />
          </Grid>
        </Grid>
        <TextField autoFocus margin="dense" id="name" label="Card Number" type="text" fullWidth />
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={4} sm={4} lg={4}>
            <TextField autoFocus margin="dense" id="name" label="Month" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={4} sm={4} lg={4}>
            <TextField autoFocus margin="dense" id="name" label="Year" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={4} sm={4} lg={4}>
            <TextField autoFocus margin="dense" id="name" label="CVV" type="text" fullWidth />
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

const YourCards = ({classes}) => {
  // const [checkedA, setCheckedA] = React.useState(false);
  // const handlechange = () => setCheckedA(!checkedA)
  const myCards = [
    {
      digit: '2342',
      Bank : 'UBA'
    },
    {
      digit: '9242',
      Bank : 'GTB'
    },
    {
      digit: '0175',
      Bank : 'FBN'
    },
  ]
  return(
    <React.Fragment>
      <List dense={true}>
      {myCards.map(({digit, Bank})=>(
        <div>
          <Divider />
          <ListItem elevation={3} style={{backgroundColor: '#dff4f3', marginTop: '5px'}}>
            <ListItemIcon>
              <Payment />
            </ListItemIcon>
            <ListItemText
              secondary={'XXXX-XXXX-XXXX-'+digit}
            />
            <ListItemSecondaryAction>
              <Button color="primary">
                {Bank}
              </Button>
              {/* <Switch checked={checkedA} onChange={handlechange} value="checkedA" color="primary"/> */}
            </ListItemSecondaryAction>
          </ListItem>
        </div> 
      ))}
        
        
      </List> 
    </React.Fragment>
  )
}

export default function FormDialog() {
  const [open, setOpen] = React.useState(false);
  const [addCard, setAddCard] = React.useState(false);
  const classes = useStyles();

  const handleClickOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const newCard = () => setAddCard(!addCard)

  return (
    <div>
      <Button variant="contained"  color="primary" onClick={handleClickOpen}>
        <Receipt />
      </Button>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          <DialogHead theCloser={handleClose}/>
        </DialogTitle>
        <DialogContent>
          <YourCards />
          <Grid container className={classes.root} spacing={1} direction="row" justify="center" alignItems="center" >
            <Grid item sm={9}>
              <Typography variant="caption" className={classes.title}>
              For a New Card Payment, fill in your details below
              </Typography>

            </Grid>
            <Grid item sm={2}>
              <Button variant="contained" color="primary" onClick={newCard}>
                <Add />
              </Button>
            </Grid>
          </Grid>
          { addCard === true ? (<TheForm classes={classes} />) : null }
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Proceed
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
