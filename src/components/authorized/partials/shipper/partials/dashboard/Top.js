import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar } from '@material-ui/core';
import { AddShipment, MakePayment} from '../../partials'
import AddContacts from '../contacts/AddContact'
import { withRouter } from 'react-router-dom';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  grow: {
    flexGrow: 1,
  },
}));

function TopHeader(props) {
  const classes = useStyles();
  const thelink = props.location.pathname

  return (
    <div className={classes.root}>
      <AppBar position="relative" elevation={0} color="transparent">
        <Toolbar className={classes.tools}>
          <div className={classes.grow} />
          {thelink === "/ShipperAddress"? <AddContacts/> :
            (
            <React.Fragment>
              <MakePayment />
              <AddShipment/>
            </React.Fragment>
            )
          }

        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withRouter(TopHeader)
