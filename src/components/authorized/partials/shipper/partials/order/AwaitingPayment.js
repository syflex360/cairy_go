import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(percel_id, type, recipient, destination, status) {
  return { percel_id, type, recipient, destination, status };
}


const rows = [
  createData('DA_12334jsdhj', 'electronic', 'Isah Rapheal', 'Canada', 'Awaiting'),
  createData('DA_12334jsdhj', 'document', 'Isah Rapheal', 'Canada', 'Awaiting'),
  createData('DA_12334jsdhj', 'document', 'Maleek John', 'Canada', 'Awaiting'),
  createData('DA_12334jsdhj', 'electronic', 'Isah Rapheal', 'Canada', 'Awaiting'),
  createData('DA_12334jsdhj', 'electronic', 'Isah Rapheal', 'Canada', 'Awaiting'),
];

export default function SimpleTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Percel ID</TableCell>
            <TableCell align="right">Type</TableCell>
            <TableCell align="right">Recipient</TableCell>
            <TableCell align="right">Destination</TableCell>
            <TableCell align="right">Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.percel_id}
              </TableCell>
              <TableCell align="right">{row.type}</TableCell>
              <TableCell align="right">{row.recipient}</TableCell>
              <TableCell align="right">{row.destination}</TableCell>
              <TableCell align="right">{row.status}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
