import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Toolbar, Button, IconButton, TextField, Dialog, DialogActions, DialogContent, DialogTitle, AppBar } from '@material-ui/core';
import { Close, Payment } from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  grow: {
    flexGrow: 1,
  },
}));

const DialogHead = (props)=>{
  const classes = useStyles();
  const {theCloser} = props;
  return(
    <div className={classes.root}>
      <AppBar position="relative" elevation={0} color="transparent">
        <Toolbar className={classes.tools}>
          <div>Add Card</div>
          <div className={classes.grow} />
          <IconButton onClick={theCloser} size="small" variant="contained" style={{ color: 'white', backgroundColor: 'red'}}>
            <Close />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  )
}

const TheForm = ({classes}) =>{
  return(
    <React.Fragment>

      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" style={{padding: '20px'}}>
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Bank" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <TextField autoFocus margin="dense" id="name" label="Account Number" type="text" fullWidth />
          </Grid>
        </Grid>
        <TextField autoFocus margin="dense" id="name" label="Card Number" type="text" fullWidth />
        <Grid container spacing={4} direction="row" justify="center">
          <Grid item xs={12} md={4} sm={4} lg={4}>
            <TextField autoFocus margin="dense" id="name" label="Month" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={4} sm={4} lg={4}>
            <TextField autoFocus margin="dense" id="name" label="Year" type="text" fullWidth />
          </Grid>
          <Grid item xs={12} md={4} sm={4} lg={4}>
            <TextField autoFocus margin="dense" id="name" label="CVV" type="text" fullWidth />
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}



export default function FormDialog() {
  const [open, setOpen] = React.useState(false);

  const classes = useStyles();

  const handleClickOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)
  // const handleClick = () => setAlert(false)




  return (
    <div>
      <Button variant="contained"  color="primary" onClick={handleClickOpen}>
        <Payment />
      </Button>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          <DialogHead theCloser={handleClose}/>
        </DialogTitle>
        <DialogContent>
          <TheForm classes={classes} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Proceed
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
