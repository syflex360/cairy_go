import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper} from '@material-ui/core';




const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(transaction_id, type, amount, charge, status) {
  return { transaction_id, type, amount, charge, status };
}

const rows = [
  createData('DA_12334jsdhj', 'Pay-Off', '4000', '0.5', 'Ongoing'),
  createData('DA_12334jsdhj', 'Pay-Off', '4000', '0.5', 'Completed'),
  createData('DA_12334jsdhj', 'Pay-Off', '4000', '0.5', 'Completed'),
  createData('DA_12334jsdhj', 'Pay-Off', '4000', '0.5', 'Completed'),
  createData('DA_12334jsdhj', 'Pay-Off', '4000', '0.5', 'Completed'),
];


export default function SimpleTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Transaction ID</TableCell>
            <TableCell align="right">Type</TableCell>
            <TableCell align="right">Amount</TableCell>
            <TableCell align="right">Charge</TableCell>
            <TableCell align="right">Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.transaction_id}
              </TableCell>
              <TableCell align="right">{row.type}</TableCell>
              <TableCell align="right">{row.amount}</TableCell>
              <TableCell align="right">{row.charge}</TableCell>
              <TableCell align="right">{row.status}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
