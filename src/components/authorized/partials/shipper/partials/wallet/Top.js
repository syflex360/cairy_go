import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, List, Divider, ListItem, ListItemIcon, ListItemText, Button, ListItemSecondaryAction} from '@material-ui/core';
import { Payment } from '@material-ui/icons'
import {AddCard} from '.'
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  grow: {
    flexGrow: 1,
  },
  large: {
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
}));

function TopHeader() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppBar position="relative" elevation={0} color="transparent">
        <Toolbar className={classes.tools}>
          <div className={classes.grow} />
          <AddCard />
        </Toolbar>
      </AppBar>
    </div>
  );
}



const YourCards = () => {
  const myCards = [
    {
      digit: '2342',
      Bank : 'UBA'
    },
    {
      digit: '9242',
      Bank : 'GTB'
    },
    {
      digit: '0175',
      Bank : 'FBN'
    },
  ]
  return(
    <React.Fragment>
      <TopHeader />
      <Divider />
      <List dense={true} style={{padding: '20px'}}>
      {myCards.map(({digit, Bank})=>(
        <div spacing={8}>

          <ListItem elevation={3} style={{backgroundColor: '#dff4f3', marginTop: '5px'}}>
            <ListItemIcon>
              <Payment />
            </ListItemIcon>
            <ListItemText
              secondary={'XXXX-XXXX-XXXX-'+digit}
            />
            <ListItemSecondaryAction>
              <Button color="primary">
                {Bank}
              </Button>
              {/* <Switch checked={checkedA} onChange={handlechange} value="checkedA" color="primary"/> */}
            </ListItemSecondaryAction>
          </ListItem>
        </div>
      ))}
      </List>
    </React.Fragment>
  )
}

export default YourCards
