
import Top from './dashboard/Top';
import AddShipment from './common/AddShipment';
import Main from './dashboard/Main';
import MakePayment from './dashboard/MakePayment';
import Singular from './contacts/singularContact';


export {
  Top, AddShipment, Main, MakePayment, Singular
}
