import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Avatar, Typography, Button, Dialog, DialogActions, DialogContent,  } from '@material-ui/core';
import { Close } from '@material-ui/icons'
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(0),
    },
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2),
  },
  grow: {
    flexGrow: 1,
  },
  large: {
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
}));

const TheDetails = ({classes}) =>{
  return(
    <React.Fragment>
      <Typography variant="h4" className={classes.title}>
        Cairy GO
      </Typography>
      <Typography variant="caption" className={classes.title}>
        1, Address my city
      </Typography>

      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={6} sm={6} lg={6}>
        <Avatar
          className={classes.large}
          src={'https://cdn3.iconfinder.com/data/icons/avatars-15/64/_Ninja-2-512.png'}
        />
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <Typography variant="h6" className={classes.title}>
            Name: John Doe
          </Typography>
        </Grid>
      </Grid>
      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={4} sm={4} lg={4}>
          <Typography className={classes.title}>
            City: Ontario
          </Typography>
        </Grid>
        <Grid item xs={12} md={4} sm={4} lg={4}>
          <Typography className={classes.title}>
            State: Ontario
          </Typography>
        </Grid>
        <Grid item xs={12} md={4} sm={4} lg={4}>
          <Typography className={classes.title}>
            Zip: 88452
          </Typography>
        </Grid>
      </Grid>
      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <Typography className={classes.title}>
            Phone: +2348122334475
          </Typography>
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <Typography className={classes.title}>
            Email: theEmail@example.com
          </Typography>
        </Grid>
      </Grid>
    </React.Fragment>

  )
}



function FormDialog(props) {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const redirector = () => {
    // console.log(props)
    setOpen(false);
    props.history.push("/ShipperAddress")
  }

  return (
    <div>
      <Button variant="outline" color="primary" onClick={handleClickOpen}>
        View
      </Button>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">

        <DialogContent>
          <TheDetails classes={classes}  />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} size="small" color="transparent" style={{ color: 'red'}}>
            <Close />
          </Button>
          <Button onClick={redirector} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default withRouter(FormDialog)
