import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Toolbar, Button, TextField,Dialog, DialogActions, DialogContent, DialogTitle, AppBar } from '@material-ui/core';
import { Add, Close } from '@material-ui/icons'
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(0),
    },
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2),
  },
  grow: {
    flexGrow: 1,
  },
}));

const TheForm = ({classes}) =>{
  return(
    <React.Fragment>
      <TextField autoFocus margin="dense" id="name" label="Recipent Company Name" type="text" fullWidth />
      <TextField autoFocus margin="dense" id="name" label="Recipent Address" type="text" fullWidth />

      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="First Name" type="text" fullWidth />
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Last Name" type="text" fullWidth />
        </Grid>
      </Grid>
      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={4} sm={4} lg={4}>
          <TextField autoFocus margin="dense" id="name" label="Recipient City" type="text" fullWidth />
        </Grid>
        <Grid item xs={12} md={4} sm={4} lg={4}>
          <TextField autoFocus margin="dense" id="name" label="Recipient State" type="text" fullWidth />
        </Grid>
        <Grid item xs={12} md={4} sm={4} lg={4}>
          <TextField autoFocus margin="dense" id="name" label="Recipient Zip" type="text" fullWidth />
        </Grid>
      </Grid>
      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Recipient Phone" type="text" fullWidth />
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Recipient Email" type="text" fullWidth />
        </Grid>
      </Grid>
    </React.Fragment>

  )
}

const DialogHead = ({theCloser, classes })=>{
  return(
    <div className={classes.root}>
      <AppBar position="relative" elevation={0} color="transparent">
        <Toolbar className={classes.tools}>
          <div>Add Contact</div>
          <div className={classes.grow} />
          <Button onClick={theCloser} size="small" color="transparent" style={{ color: 'red'}}>
            <Close />
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  )
}

function FormDialog(props) {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const redirector = () => {
    // console.log(props)
    setOpen(false);
    props.history.push("/ShipperAddress")
  }

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleClickOpen}>
        <Add />
        Add Contact
      </Button>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          <DialogHead theCloser={handleClose} classes={classes}/>

        </DialogTitle>
        <DialogContent>
          <TheForm classes={classes}  />
        </DialogContent>
        <DialogActions>
          <Button onClick={redirector} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default withRouter(FormDialog)
