import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Card, Button, TextField, CardHeader, CardContent } from '@material-ui/core';
import { withRouter, Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(0),
    },
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2),
  },
  grow: {
    flexGrow: 1,
  },
  horizontal:{
    display : 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'column',
    textTransform: 'capitalize',
    padding: theme.spacing(5),
  }
}));

const TheForm = ({classes}) =>{
  return(
    <React.Fragment>
      <TextField autoFocus margin="dense" id="name" label="email" type="text" fullWidth />
      <TextField autoFocus margin="dense" id="name" label="Password" type="text" fullWidth />
    </React.Fragment>

  )
}


function FormDialog(props) {
  const classes = useStyles();

  return (
    <div className={`${classes.horizontal}`}>
      <Card className={`${classes.paper} ${classes.horizontal}`} elevation={3} style={{maxWidth: '800px'}}>
        <CardHeader title="Forgot Password" />
        <CardContent>
          <TheForm classes={classes}  />
          <Grid container spacing={4} direction="row" justify="center" alignItems="center" style={{marginTop: '10px'}}>
            <Link to="/TravellerDashboard" style={{color: 'white'}}>
              <Button variant="contained" size="small" color="primary" style={{ color: 'white', textTransform: 'capitalize', marginRight: '5px'}} >
                Login
              </Button>
            </Link>
          </Grid>
        </CardContent>

        <div className={classes.horizontal}>
        <Grid container className={classes.root} spacing={2} direction="row" justify="center" alignItems="center" >
          <Link to="/Register" style={{color: 'white'}}>
            <Button size="small" color="primary" style={{ textTransform: 'capitalize', marginRight: '5px'}}>
              Sign Up
            </Button>
          </Link>
          <Link to="/Forgot" style={{color: 'white'}}>
            <Button size="small" color="primary" style={{ textTransform: 'capitalize', marginRight: '5px'}}>
              Forgot password ?
            </Button>
          </Link>
        </Grid>


        </div>
      </Card>
    </div>
  );
}

export default withRouter(FormDialog)
