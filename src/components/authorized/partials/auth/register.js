import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Card, Button, TextField, CardHeader, CardContent } from '@material-ui/core';
// import { Add, Close } from '@material-ui/icons'
import { withRouter, Link } from 'react-router-dom';
// import SignUp from './register'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(0),
    },
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2),
  },
  grow: {
    flexGrow: 1,
  },
  horizontal:{
    display : 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'column',
    textTransform: 'capitalize',
    padding: theme.spacing(5),
  }
}));

const TheForm = ({classes}) =>{
  return(
    <React.Fragment>
      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="First Name" type="text" fullWidth />
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Last Name" type="text" fullWidth />
        </Grid>
      </Grid>
      <TextField autoFocus margin="dense" id="name" label="email" type="text" fullWidth />
      <TextField autoFocus margin="dense" id="name" label="phone" type="text" fullWidth />
      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Password" type="text" fullWidth />
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Confirm Password" type="text" fullWidth />
        </Grid>
      </Grid>
    </React.Fragment>

  )
}


function FormDialog(props) {
  const classes = useStyles();

  return (
    <div className={`${classes.horizontal}`}>
      <Card className={`${classes.paper} ${classes.horizontal}`} elevation={3} style={{maxWidth: '800px'}}>
        <CardHeader title="Sign Up" />
        <CardContent>
          <TheForm classes={classes}  />
          <Grid container spacing={4} direction="row" justify="center" alignItems="center" style={{padding: '10px'}}>
            <Link to="/TravellerDashboard" style={{color: 'white'}}>
              <Button variant="contained" size="small" color="primary" style={{ color: 'white', textTransform: 'capitalize', marginRight: '5px'}} >
                Sign Up
              </Button>
            </Link>
          </Grid>
        </CardContent>

        <div className={classes.horizontal}>
        <Grid container className={classes.root} spacing={2} direction="row" justify="center" alignItems="center" >
          <Link to="/Login" style={{color: 'white'}}>
            <Button size="small" color="primary" style={{ textTransform: 'capitalize', marginRight: '5px'}}>
            Login
            </Button>
          </Link>
          <Link to="/Forgot" style={{color: 'white'}}>
            <Button size="small" color="primary" style={{ textTransform: 'capitalize', marginRight: '5px'}}>
              Forgot password ?
            </Button>
          </Link>
        </Grid>


        </div>
      </Card>
    </div>
  );
}

export default withRouter(FormDialog)
