import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Card, Button, TextField, CardHeader, CardContent, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import { withRouter, Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(0),
    },
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2),
  },
  grow: {
    flexGrow: 1,
  },
  horizontal:{
    display : 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'column',
    textTransform: 'capitalize',
    padding: theme.spacing(5),
  }
}));

const TheForm = () =>{
  return(
    <React.Fragment>
      <h5 >Put in your email here</h5>
      <TextField autoFocus margin="dense" id="name" label="email" type="text" fullWidth />
    </React.Fragment>
  )
}

const TheDialog = () => {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  return (
    <React.Fragment>
      <Button onClick={handleClickOpen} variant="contained" size="small" color="primary" style={{ color: 'white', textTransform: 'capitalize', marginRight: '5px'}} >
        Send
      </Button>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogContent>
          <h4 >Yoour Password has been sent to your Email</h4>
        </DialogContent>
        <DialogActions>
        <Link to="/TravellerDashboard" style={{color: 'white'}}>
            <Button onClick={handleClose} variant="contained" size="small" color="primary" style={{ color: 'white', textTransform: 'capitalize', marginRight: '5px'}} >
              Done
            </Button>
          </Link>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
}


function FormDialog(props) {
  const classes = useStyles();

  return (
    <div className={`${classes.horizontal}`}>
      <Card className={`${classes.paper} ${classes.horizontal}`} elevation={3} style={{maxWidth: '800px'}}>
        <CardHeader title="Forgot Password" />
        <CardContent>
          <TheForm classes={classes}  />
          <Grid container spacing={4} direction="row" justify="center" alignItems="center" style={{marginTop: '10px'}}>
            <TheDialog />
          </Grid>
        </CardContent>

        <div className={classes.horizontal}>
        <Grid container className={classes.root} spacing={2} direction="row" justify="center" alignItems="center" >
          <Link to="/Register" style={{color: 'white'}}>
            <Button size="small" color="primary" style={{ textTransform: 'capitalize', marginRight: '5px'}}>
              Sign Up
            </Button>
          </Link>
          <Link to="/Login" style={{color: 'white'}}>
            <Button size="small" color="primary" style={{ textTransform: 'capitalize', marginRight: '5px'}}>
              Login
            </Button>
          </Link>
        </Grid>


        </div>
      </Card>
    </div>
  );
}

export default withRouter(FormDialog)
