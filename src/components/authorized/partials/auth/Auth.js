import React from 'react';
// import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { Login, Register, Forgot } from './';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

function SimpleTabs(props) {
  const classes = useStyles();
  const thelink = props.location.pathname

  const TheAuth = () => {
    if(thelink === "/Login"){
      return <Login />
    }else if(thelink === "/Register"){
      return <Register />
    }else if(thelink === "/Forgot"){
      return <Forgot />
    }
  }

  return (
    <div className={classes.root}>
      <TheAuth />
    </div>
  );
}

export default withRouter(SimpleTabs)
