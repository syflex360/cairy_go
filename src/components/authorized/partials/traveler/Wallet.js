import React from 'react';
// import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
// import { AppBar, Tabs, Tab, Typography, Box } from '@material-ui/core';
// import Trip from './partials/trips/Trip';
import { Top, Payout } from './partials/Wallet'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function SimpleTabs() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Top />
      <Payout/>

    </div>
  );
}
