import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Top, Main } from './partials/dashboard'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Top />
      <Main />
    </div>
  );
}

