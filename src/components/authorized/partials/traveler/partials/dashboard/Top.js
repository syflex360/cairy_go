import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar } from '@material-ui/core';
import AddTrip from '../common/AddTrip'
import Withdraw from './Withdraw'


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  grow: {
    flexGrow: 1,
  },
}));

export default function TravellerHeader() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="relative" elevation={0} color="transparent">
        <Toolbar className={classes.tools}>
          <div className={classes.grow} />
          <Withdraw />
          <AddTrip/>
        </Toolbar>
      </AppBar>
    </div>
  );
}

