import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Toolbar, Button, IconButton, TextField,Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, AppBar } from '@material-ui/core';
import { Close, Payment } from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  grow: {
    flexGrow: 1,
  },
}));

const DialogHead = (props)=>{
  const classes = useStyles();
  const {theCloser} = props;
  return(
    <div className={classes.root}>
      <AppBar position="relative" elevation={0} color="transparent">
        <Toolbar className={classes.tools}>
          <div>Withdraw</div>
          <div className={classes.grow} />
          <IconButton onClick={theCloser} size="small" variant="contained" style={{ color: 'white', backgroundColor: 'red'}}>
            <Close />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default function FormDialog() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained"  color="primary" onClick={handleClickOpen}>
        <Payment />
      </Button>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          <DialogHead theCloser={handleClose}/>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Type in Whatever amount you want to withdraw
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Amount"
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Proceed
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
