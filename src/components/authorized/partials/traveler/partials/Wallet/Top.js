import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemAvatar, Avatar, ListItemText} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  grow: {
    flexGrow: 1,
  },
  large: {
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
}));





const YourCards = () => {
  const classes = useStyles()
  return(
    <React.Fragment>
      <List dense={true}>

        <ListItem elevation={3} style={{marginBottom: '5px'}}>
          <ListItemAvatar style={{paddingRight: '10px'}}>
            <Avatar
              className={classes.large}
              src={'https://cdn3.iconfinder.com/data/icons/avatars-15/64/_Ninja-2-512.png'}
            />
          </ListItemAvatar>
          <ListItemText
            primary = 'John found Joe'
            secondary='0012345678'
          />
          <ListItemText
            primary = 'N200,000'
            secondary='UBA'
          />
        </ListItem>
      </List>
    </React.Fragment>
  )
}

export default YourCards
