
import Top from './Top';
import Settings from './settings';
import History from '../History';
import Payout from './Payout';

export {
  Top, History, Payout, Settings
}
