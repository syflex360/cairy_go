import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(trip_id, depature, arrival, parcel, status) {
  return { trip_id, depature, arrival, parcel, status };
}


const rows = [
  createData('DA_12334jsdhj', 'Ahmadu Bello Airport Lagos, Nigeria', 'JFK Airport NY, USA', 'electronic', 'Completed'),
  createData('DA_12334jsdhj', 'Ahmadu Bello Airport Lagos, Nigeria', 'JFK Airport NY, USA', 'electronic', 'Completed'),
  createData('DA_12334jsdhj', 'Ahmadu Bello Airport Lagos, Nigeria', 'JFK Airport NY, USA', 'electronic', 'Completed'),
  createData('DA_12334jsdhj', 'Ahmadu Bello Airport Lagos, Nigeria', 'JFK Airport NY, USA', 'electronic', 'Completed'),
  createData('DA_12334jsdhj', 'Ahmadu Bello Airport Lagos, Nigeria', 'JFK Airport NY, USA', 'electronic', 'Completed'),
];


export default function SimpleTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Trip ID</TableCell>
            <TableCell align="right">Depature</TableCell>
            <TableCell align="right">Arrival</TableCell>
            <TableCell align="right">Parcel</TableCell>
            <TableCell align="right">Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.trip_id}
              </TableCell>
              <TableCell align="right">{row.depature}</TableCell>
              <TableCell align="right">{row.arrival}</TableCell>
              <TableCell align="right">{row.parcel}</TableCell>
              <TableCell align="right">{row.status}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
