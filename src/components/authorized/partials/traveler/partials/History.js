import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Grid,  Card , Typography, CardActions, CardContent, Divider, Button} from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import {routLink} from '../../../../mixings'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(6),
  },
  paper: {
    textAlign: 'center',
    color: 'white',
    margin: 'auto',
    height: 120,
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(2)
  },
  control: {
    padding: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    verticalAlign: 'middle'
  },
}));




function SpacingGrid(props) {
  const classes = useStyles();
  const data = {
    boards:[
      {
        thename: 'Trips',
        thecolor: '#6639a6',
        link: ()=>{routLink(props, "/TravellerDashboard")}
      },
      {
        thename: 'Contact',
        thecolor: '#3490de',
        link: ()=>{routLink(props, "/TravellerDashboard")}
      },
      {
        thename: 'Wallet',
        thecolor: '#07689f',
        link: ()=>{routLink(props, "/TravellerDashboard")}
      },
    ],
    gridItem: {
      col: 4,
      color: 'white'
    }
  }

  return (
    <React.Fragment>
      <Grid
      container
      className={classes.root}
      spacing={4}
      direction="row" justify="center" alignItems="center"
    >
    {data.boards.map( ({ thename, thecolor, link }) => (
      <Grid item xs={12} md={data.gridItem.col} sm={data.gridItem.col} lg={data.gridItem.col}>
        <Card onClick={link} className={classes.paper} elevation={3} style={{ background: thecolor}}>
          <CardContent>
            <Typography variant="h3" className={classes.title}>
              {thename}
            </Typography>
          </CardContent>

          <Divider style={{ background: 'white'}}/>
          <CardActions>
            <Button size="small" variant="outlined" color="error" style={{ color: 'white', textTransform: 'capitalize'}} >
              Learn More
            </Button>
          </CardActions>
        </Card>
      </Grid>
      ))}
    </Grid>
    </React.Fragment>

  );
}

export default withRouter(SpacingGrid)
