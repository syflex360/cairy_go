import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemSecondaryAction, ListItemText, Typography, Switch, Grid, Toolbar, Button, TextField,Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, AppBar } from '@material-ui/core';
import { Add, Close } from '@material-ui/icons'
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  tools: {
    '& > *': {
      margin: theme.spacing(0),
    },
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2),
  },
  grow: {
    flexGrow: 1,
  },
}));

class StopOver extends React.Component {

  state = {
    checkedA: false,
  }

  handlechange = () => {
    this.setState({
      checkedA: !this.state.checkedA
   })
  }
  render() {
    const {classes} = this.props
    const {checkedA} = this.state
    return(
      <React.Fragment>
        <div className={classes.demo} style={{backgroundColor: '#d3d3d3'}}>
          <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
            <Grid item sm>
              <TextField autoFocus margin="dense" id="name" label="Departure Time" type="text" fullWidth />
            </Grid>
            <Grid item sm>
              <TextField autoFocus margin="dense" id="name" label="Departure Airport" type="text" fullWidth />
            </Grid>
          </Grid>
          <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
            <Grid item sm>
              <TextField autoFocus margin="dense" id="name" label="Arrival Time" type="text" fullWidth />
            </Grid>
            <Grid item sm>
              <TextField autoFocus margin="dense" id="name" label="Arrival Airport" type="text" fullWidth />
            </Grid>
          </Grid>
          <List dense={true}>
            <ListItem>
              <ListItemText
                secondary='Is there an Airline Change?'
              />
              <ListItemSecondaryAction>
                <Switch checked={checkedA} onChange={this.handlechange} value="checkedA" color="primary"/>
              </ListItemSecondaryAction>
            </ListItem>
            <ListItem>
            { checkedA === true ? (
              <React.Fragment>
                <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
                  <Grid item sm>
                    <TextField autoFocus margin="dense" id="name" label="Name of Airline" type="text" fullWidth />
                  </Grid>
                  <Grid item sm>
                    <TextField autoFocus margin="dense" id="name" label="Flight No." type="text" fullWidth />
                  </Grid>
                </Grid>
              </React.Fragment>
              ) : null }
            </ListItem>
          </List>


        </div>

      </React.Fragment>
    )
  }
}



const TheForm = ({classes}) =>{
  return(
    <React.Fragment>
      <TextField autoFocus margin="dense" id="name" label="Airline" type="text" fullWidth />
      <TextField autoFocus margin="dense" id="name" label="Flight Number" type="text" fullWidth />

      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Departure Airport" type="text" fullWidth />
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Departure Time" type="text" fullWidth />
        </Grid>
      </Grid>
      <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Arrival Airport" type="text" fullWidth />
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <TextField autoFocus margin="dense" id="name" label="Arrival Time" type="text" fullWidth />
        </Grid>
      </Grid>
    </React.Fragment>

  )
}

const DialogHead = ({theCloser, classes })=>{
  return(
    <div className={classes.root}>
      <AppBar position="relative" elevation={0} color="transparent">
        <Toolbar className={classes.tools}>
          <div>Add Trip</div>
          <div className={classes.grow} />
          <Button onClick={theCloser} size="small" color="transparent" style={{ color: 'red'}}>
            <Close />
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  )
}

function FormDialog(props) {
  const [open, setOpen] = React.useState(false);
  const [stopOver, setStopOver] = React.useState(false);


  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const stopOverChange = () => {
    setStopOver(!stopOver)
  }

  const redirector = () => {
    // console.log(props)
    setOpen(false);
    props.history.push("/TravellerTrips")
  }

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleClickOpen}>
        <Add />
        Add Trip
      </Button>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          <DialogHead theCloser={handleClose} classes={classes}/>

        </DialogTitle>
        <DialogContent>
          <DialogContentText> Put in your Flight Details here </DialogContentText>
          <TheForm classes={classes}  />
          <Grid container className={classes.root} spacing={4} direction="row" justify="center" alignItems="center" >
            <Grid item sm>
              <Typography variant="caption" className={classes.title}>
                Are you expecting a stop over?
              </Typography>

            </Grid>
            <Grid item sm>
              <Switch checked={stopOver} onChange={stopOverChange} value="stopOver" color="primary"/>
            </Grid>
          </Grid>
          { stopOver === true ? (<StopOver classes={classes} />) : null }

        </DialogContent>
        <DialogActions>
          <Button onClick={redirector} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default withRouter(FormDialog)
