import React from 'react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { makeStyles } from '@material-ui/core/styles';
import { Grid,  Card , Typography, CardContent, Divider, Button } from '@material-ui/core';
import { FlightTakeoff, FlightLand, Search, CloudUploadOutlined } from '@material-ui/icons'
import { withRouter, Link } from 'react-router-dom';
// import {routLink} from '../mixings'

const classes = {
  root: {
    flexGrow: 0,
  },
  heading: {
    fontWeight : 'bolder'
  },
  header: {
    backgroundImage: 'url(https://images.unsplash.com/photo-1464038008305-ee8def75f234?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80)',
    height: '60vh',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover'
  },
  content: {
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    color: 'white',
    display : 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'column',
    textAlign: 'center',
  },
  btn:{
    color: 'white',
    textTransform: 'capitalize'
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  paper: {
    textAlign: 'center',
    color: 'white',
    margin: 'auto',
    height: 120,
    display : 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'column'
  },
  control: {
    padding: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    verticalAlign: 'middle'
  },
  horizontal:{
    display : 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'column',
    textTransform: 'capitalize'
  }
}));

const HowItWorks = () => {
  const myclasses = useStyles();
  const data = {
    boards:[
      {
        icon: <CloudUploadOutlined fontSize="large"/>,
        thename: 'Upload flight details',
        thecolor: '#6639a6',
      },
      {
        icon: <Search fontSize="large"/>,
        thename: 'Find sender or be found ',
        thecolor: '#3490de',
      },
      {
        icon: <FlightTakeoff fontSize="large"/>,
        thename: 'Accept package from departure airport',
        thecolor: '#07689f',
      },
      {
        icon: <FlightLand fontSize="large"/>,
        thename: 'Deliver package at destination airport',
        thecolor: '#f35588',
      },
    ],
    gridItem: {
      col: 3,
      color: 'white'
    }
  }
  return(
    <React.Fragment>
      <h2 className={myclasses.horizontal}> Here's How it Works </h2>
      <Grid
      container
      className={myclasses.root}
      spacing={2}
      direction="row" justify="center" alignItems="center"
    >
    {data.boards.map( ({ thename, thecolor, icon }) => (
      <Grid item xs={12} md={data.gridItem.col} sm={data.gridItem.col} lg={data.gridItem.col}>

        <Card className={myclasses.paper} elevation={3} style={{ background: thecolor}}>
          <CardContent>
            <div>{icon}</div>
            <Divider />
            <Typography variant="caption" className={myclasses.title}>
              {thename}
            </Typography>
          </CardContent>
        </Card>
      </Grid>
      ))}
    </Grid>
    </React.Fragment>
  )
}
class IndexPage extends React.Component {
  render(){
    const settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
    };
    return (
      <React.Fragment>
        <Slider {...settings}>
          <div >
            <div style={classes.header}>
              <div style={classes.content}>
                <FlightTakeoff fontSize="large"/>
                <h1 >Make money travelling</h1>
                <div style={{justifyContent: 'spaceBetween'}}>
                <Link to="/Register" style={{color: 'white'}}>
                  <Button variant="outlined" size="small" color="inherit" style={{ color: 'white', textTransform: 'capitalize', marginRight: '5px'}}>
                    Sign Up
                  </Button>
                </Link>

                <Link to="/Login" style={{color: 'white'}}>
                  <Button variant="contained" size="small" color="primary" style={{ color: 'white', textTransform: 'capitalize', marginLeft: '5px'}}>
                    Login
                  </Button>
                </Link>
                </div>
              </div>
            </div>
          </div>
        </Slider>
        <HowItWorks />

     </React.Fragment>
    );
  }
}

export default withRouter(IndexPage)
