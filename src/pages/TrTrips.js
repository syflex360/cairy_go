import React, { Fragment } from "react";
import {Component} from "react";
import "../assets/css/App.css";
import { Header} from '../components/layout/index'
import Trips from '../components/authorized/partials/traveler/Trips'



export default class extends Component {
  render(){
    return (
      <Fragment>
        <Header/>
        <Trips />
     </Fragment>
    );
  }
}

