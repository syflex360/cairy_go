import App from "./App";
import ShipperAddress from "./ShAddress";
import ShipperDashboard from "./ShDashboard";
import ShipperShipment from "./ShShipment";
import ShipperWallet from "./ShWallet";
import TravellerDashboard from "./TrDashboard";
import TravellerTrips from "./TrTrips";
import TravellerWallet from "./TrWallet";
import AuthPage from "./AuthPage";

export {
  App, ShipperAddress, ShipperDashboard, ShipperShipment, ShipperWallet, TravellerDashboard, TravellerTrips, TravellerWallet, AuthPage
}

