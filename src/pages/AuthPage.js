import React, { Fragment } from "react";
import {Component} from "react";
import "../assets/css/App.css";
import { Header} from '../components/layout/index'
import Auth from '../components/authorized/partials/auth/Auth'



export default class extends Component {
  render(){
    return (
      <Fragment>
        <Header/>
        <Auth/>
      </Fragment>
    );
  }
}

