import React, { Fragment } from "react";
import {Component} from "react";
import "../assets/css/App.css";
import { Header} from '../components/layout/index'
import Traveler from '../components/authorized/partials/traveler/index'



export default class extends Component {
  render(){
    return (
      <Fragment>
        <Header/>
        <Traveler />
     </Fragment>
    );
  }
}

